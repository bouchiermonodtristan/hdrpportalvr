# A VR Portal in Unity HDRP

[The Sebastian Lague youtube video explain the project](https://www.youtube.com/watch?v=cWpFZbjtSQg)

This repository does two thing. It port it to HDRP and it port it to VR.
Actually the desktop mode works on HDRP, but the VR mode doesn't yet.
This project is based on the Sebastian Lague work that can be found and downloaded on his [github](https://github.com/SebLague/Portals/tree/master) for the standard unity pipeline.

![Demo Scene Desktop](Docs/Images/portal.JPG)

## Changelog

 - 2020-09-12
   - First Version Working on Desktop mode on HDRP
	- [Unity Package](https://drive.google.com/file/d/1FKFWBjvz8_vzh89_C3C2K4t53G38WDmW/view?usp=sharing)
   
## Project settings :
Unity 2019.4.16f 
HDRP 7.4.3

## Setup

VR on single pass.
The scene is under Assets/Scenes/PortalVR

You can change the mode (Desktop or MockHDM) to simulate VR or not by going into Edits/Project Settings/XR Plug-in Management 
and check or uncheck the Initialize XR on Startup. Pressing play get you into it.
![Unity Configuration](Docs/Images/HMD.JPG)


## Desktop Working & Gif

The desktop mode is actually working as seen here :

![Demo Scene Desktop](Docs/Images/portal.JPG)

And the gif can be found here
[![Image from Gyazo](https://i.gyazo.com/ba957cc248c9b7885ab4362dc5aeac63.gif)](https://gyazo.com/ba957cc248c9b7885ab4362dc5aeac63)
 

## VR Not Working & Gif

The VR mode is actually not working as seen here :

![Demo Scene Desktop](Docs/Images/portalVR.JPG)

And the gif can be found here 
[![Image from Gyazo](https://i.gyazo.com/a9f60b2a43bf315498c832e6a685e18d.gif)](https://gyazo.com/a9f60b2a43bf315498c832e6a685e18d)

It's noticeable that the content of the portal frame doesn't represent properly what the player should see like on the desktop one.


## Components and Scene description


 1. The video will help you to get a basic knownledge of what's going on : [Watch video](https://www.youtube.com/watch?v=cWpFZbjtSQg)
 2. The script Portal/Scripts/Core/MainCamera.cs is attached to the main camera under the player and launch on late update the Render() function of the scripts Portal.cs
 3. The script Portal/Scripts/Core/Portal.cs is on both portals and is responsible for moving the portals cameras as the player move around. This script also make the RenderTexture for the portal Camera and trigger the camera.Render() that render on the RenderTexture.
 4. For both portals a Screen child GameObject contain a mesh renderer with a Material having the specific shader made with shadergraph which is very basic, and only take the screenPosition, and the Camera render texture to print on the portal screen. The shader is under Portal/Scripts/Core/SheyneVRPortal

![Shader](Docs/Images/shadergraph.JPG)


## Support Thread
 
You can use the [unity support main thread](https://forum.unity.com/threads/trying-to-get-an-hdrp-portal-to-work-on-vr.1017616/) if you make some improvement or come out with idea to help.